﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coincon : MonoBehaviour
{
    public static int totalCoins = 0;
    public static int continueCoins;
    public AudioSource coinAudio;

    void Start()
    {
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 3, 0);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            totalCoins += 1;
            // coinAudio = GetComponent<AudioSource>();
            // coinAudio.Play(0);
            // //Debug.Log(totalCoins);
            // Destroy(gameObject);
            //coinAudio.Play();
            Destroy (this.gameObject);
            LevelManager.instance.coinCount++;
            
            // continueCoins += LevelManager.coinCount;
            // Debug.Log("continueCoins");
            // Debug.Log(continueCoins);
        }
    }

    // void OnCollisionEnter(Collision other)
    // {
    //     if (other.collider.tag  == "Player")
    //     {
    //         totalCoins += 1;
    //         Destroy (this.gameObject);
    //         LevelManager.instance.coinCount++;
    //     }
    // }

    
}
