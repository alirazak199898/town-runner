using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Manu : MonoBehaviour
{
    private string input;
    const string URL = "http://api.tenenet.net";
    const string token = "1dcc9e5923f189c907ec5a837d430a92";
    public static string alias;
    private string fname;
    private string lname;
    private string id = "scoreLBoard";
    string jsonString;

    public void PlayGame() 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void inputName(string name)
    {
        Debug.Log(name);
        StartCoroutine(getPlayer(name));
    }

    public void QuitGame () 
    {
        Debug.Log("quit");
        Application.Quit();
    }

    public string extractJson(string formData)
    {
        string[] extractData = formData.Split(new char[] { '"' }, System.StringSplitOptions.RemoveEmptyEntries);

        for (int i = 0; i < extractData.Length; i++)
        {
            if (extractData[i] == "status")
                jsonString = extractData[i + 2];
        }
        Debug.Log(jsonString);
        return jsonString;
    }

    private IEnumerator getPlayer(string name)
    {
        alias = name;
        UnityWebRequest www = UnityWebRequest.Get(URL + "/getPlayer" + "?token=" + token + "&alias=" + alias);
        yield return www.SendWebRequest();
        Debug.Log("HERE");
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            string jsonForm = www.downloadHandler.text;
            string status = extractJson(jsonForm);

            if (status == "0")
            {
                StartCoroutine(registerNewPlayer(name));
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }

    private IEnumerator registerNewPlayer(string name)
    {
        alias = name;
        fname = name;
        lname = name;
        UnityWebRequest www = UnityWebRequest.Get(URL + "/createPlayer" + "?token=" + token + "&alias=" + alias + "&id=" + id + "&fname=" + fname + "&lname=" + lname);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
           Debug.Log(www.error);
        }
        else
        {
           Debug.Log(www.downloadHandler.text);
        }
    }

    
}
