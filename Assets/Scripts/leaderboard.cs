
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
 
public class leaderboard : MonoBehaviour
{
    public Text scorelist1;
    public Text scorelist2;
    public Text scorelist3;
    //public Text scorelist4;
    //public Text scorelist5;
 
    const string URL = "http://api.tenenet.net";
    const string token = "1dcc9e5923f189c907ec5a837d430a92";
    private string id = "scoreLBoard";
    private string lboardJson;
    private string rankJson;
    private int limitPlayer;
    private int count = 1;
    SortedList<double, string> listName = new SortedList<double, string>();
 
    public void backButton()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame ()
    {
        Application.Quit ();
    }

    public void newGameButton()
    {
        SceneManager.LoadScene(0);
    }

    private void Start()
    {
        StartCoroutine(getleaderboard());
    }
 
    private IEnumerator getleaderboard()
    {
        UnityWebRequest www = UnityWebRequest.Get(URL + "/getLeaderboard" + "?token=" + token + "&id=" + id);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            lboardJson = www.downloadHandler.text;
        }
        var leadeboardJSON = JsonConvert.DeserializeObject<leaderBoard>(lboardJson);
        limitPlayer = leadeboardJSON.message.data.Count;
        Debug.Log(limitPlayer);
 
        for (int i = 0; i < limitPlayer; i++)
        {
            //System.Threading.Thread.Sleep(1000);
            string aliasName = leadeboardJSON.message.data[i].alias.ToString();
            StartCoroutine(getPlayer(aliasName));
        }
    }
    private IEnumerator getPlayer(string alias)
    {
        UnityWebRequest www = UnityWebRequest.Get(URL + "/getPlayer" + "?token=" + token + "&alias=" + alias);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            rankJson = www.downloadHandler.text;
        }
        var scoreJson = JsonConvert.DeserializeObject<gpRoot>(rankJson);
        var score = scoreJson.message.score[0].value;
        var name = scoreJson.message.alias;
        double intscore = double.Parse(score);
        listName.Add(intscore, name);
        // if (count == 5)
        // {
        //     scorelist1.text = "1.\t" + listName.Values[4] + "\t\t- " + listName.Keys[4];
        //     scorelist2.text = "2.\t" + listName.Values[3] + "\t\t- " + listName.Keys[3];
        //     scorelist3.text = "3.\t" + listName.Values[2] + "\t\t- " + listName.Keys[2];
        //     scorelist4.text = "4.\t" + listName.Values[1] + "\t\t- " + listName.Keys[1];
        //     scorelist5.text = "5.\t" + listName.Values[0] + "\t\t- " + listName.Keys[0];
        // }
        // if (count == 4)
        // {
        //     scorelist1.text = "1.\t" + listName.Values[3] + "\t\t- " + listName.Keys[3];
        //     scorelist2.text = "2.\t" + listName.Values[2] + "\t\t- " + listName.Keys[2];
        //     scorelist3.text = "3.\t" + listName.Values[1] + "\t\t- " + listName.Keys[1];
        //     scorelist4.text = "4.\t" + listName.Values[0] + "\t\t- " + listName.Keys[0];
        // }
        if (count == 3)
        {
            scorelist1.text = listName.Values[2] + "\t-\t" + listName.Keys[2];
            scorelist2.text = listName.Values[1] + "\t-\t" + listName.Keys[1];
            scorelist3.text = listName.Values[0] + "\t-\t" + listName.Keys[0];
        }
        if (count == 2)
        {
            scorelist1.text = listName.Values[1] + "\t-\t" + listName.Keys[1];
            scorelist2.text = listName.Values[0] + "\t-\t" + listName.Keys[0];
        }
        if (count == 1)
        {
            scorelist1.text = listName.Values[0] + "\t-\t" + listName.Keys[0];
        }
        count++;
    }
 
    public void menu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 4);
    }
 
    public class gpScore
    {
        public string metric_id { get; set; }
        public string metric_name { get; set; }
        public string metric_type { get; set; }
        public string value { get; set; }
    }
    public class gpMessage
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string alias { get; set; }
        public string id { get; set; }
        public string created { get; set; }
        public List<gpScore> score { get; set; }
    }
    public class gpRoot
    {
        public gpMessage message { get; set; }
        public string status { get; set; }
    }
    public class lbDatum
    {
        public string alias { get; set; }
        public int rank { get; set; }
    }       
    public class lbMessage
    {
        public List<lbDatum> data { get; set; }
    }
    public class leaderBoard
    {
        public lbMessage message { get; set; }
        public string status { get; set; }
    }
}