using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menus : MonoBehaviour
{
    public PlayerController player;
    public GameObject pauseMenu;
    //public Static Menus _instance;

    public void PauseGame () 
    {
        if(!player.isDead){
            pauseMenu.SetActive (true);
            Time.timeScale = 0;
            //LevelManager.instance.isPaused = true;
        }
        
    }

    public void ResumeGame ()
    {
        if(!player.isDead){
            pauseMenu.SetActive (false);
            Time.timeScale = 1;
            //LevelManager.instance.isPaused = false;
        }
    }

    public void RestartGame ()
    {
        Time.timeScale = 1;
        Application.LoadLevel (0);
        
    }

    public void QuitGame ()
    {
        Application.Quit ();
    }

    public void Exit ()
    {
        Application.Quit ();
    }
}
