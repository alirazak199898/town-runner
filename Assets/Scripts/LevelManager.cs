using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;
    private int _framePaused = 0;

    public GameObject player;
    public Text Score;
    public float playerScore;
    public static double scoreP;
    public bool isPaused;
    public int coinCount = 0;
    public Text txtCoin;
    public Image imgCoinProgress;
    public Text txtMultiplier;
    public int multiplier = 1;
    public float fillAmount = 10;
    public int heartCount = 0;
    public Text txtHeart;
    public Text txtContinueHeart;
    public double distance = 0;
    public int distanceFactor = 200;
    public Text txtDistance;
    public Animator distanceAnimator;
    private int total = 0;
    //public GameObject GameOverUI;
    //public bool isDead = false;

    public static LevelManager instance
    {
        get {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<LevelManager> ();
                }
                return _instance;
            }
    }

    void Start()
    {
        playerScore = 0;
        isPaused = false;
    }

    void Update()
    {
        CoinCount();
        ScoreCalculation();
        HeartCount();
    }

    private void CoinCount()
    {
        txtCoin.text = coinCount.ToString();
        txtMultiplier.text = multiplier.ToString();

        if (imgCoinProgress.fillAmount == 1)
        {
            imgCoinProgress.fillAmount = 0;
            multiplier++;
        }
        else
        {
            imgCoinProgress.fillAmount = (coinCount - (fillAmount * (multiplier - 1))) / fillAmount;
        }
    }

    private void ScoreCalculation()
    {
        if (!isPaused && !player.GetComponent<PlayerController>().isDead)
        {
            // playerScore = (Time.frameCount - _framePaused) / 5f;
            // float c = 0.1 * coinCount;
            // playerScore = playerScore * c;
            // Score.text = ("SCORE:" + (int)playerScore).ToString();

            playerScore = (Time.frameCount - _framePaused) / 10;
            double playerScore2 = (Time.frameCount - _framePaused);
            double c = 0.5 * coinCount;
            scoreP = playerScore + (playerScore * c);
            Score.text = ("SCORE:" + (int)scoreP).ToString();

            if (playerScore2 % distanceFactor == 0)
            {
                distance += 100;
                // if ((coincon.totalCoins % 5 == 0) && (total == 0) && (coincon.totalCoins > 4))
                // {
                //     distance = distance * 1.1;
                // }

                // if ((coincon.totalCoins % 5 != 0) && (total == 1) && (coincon.totalCoins > 4))
                // {
                //     total = 0;
                // }
                txtDistance.text = distance + "m";
                StartCoroutine(ShowDistance());
            }
            // txtCoin.text = coinCount.ToString();
        }
        else if (isPaused)
        {
            _framePaused++;
        }  
    }

    private IEnumerator ShowDistance()
    {
        distanceAnimator.SetTrigger("Show");
        yield return new WaitForSeconds(2);
        distanceAnimator.SetTrigger("Hide");
    }

    private void HeartCount ()
    {
        txtHeart.text = heartCount.ToString();
    }
    
    public void SaveScore(){
        int HighestScore = PlayerPrefs.GetInt("HighestScore");
        //Debug.Log("HighestScore");
        //Debug.Log(HighestScore);
        if (playerScore > HighestScore){
            PlayerPrefs.SetInt("HighestScore", (int)playerScore);
        }
    }

    public void SaveCoins(){
        int CollectedCoins = PlayerPrefs.GetInt("CollectedCoins");
        //Debug.Log("CollectedCoins");
        //Debug.Log(CollectedCoins);
        CollectedCoins = coinCount + CollectedCoins;
        PlayerPrefs.SetInt("CollectedCoins", CollectedCoins);
    }

    public void SaveHearts(){
        int CollectedHearts = PlayerPrefs.GetInt("CollectedHearts");
        CollectedHearts = heartCount + CollectedHearts;
        PlayerPrefs.SetInt("CollectedHearts", CollectedHearts);
        //Debug.Log("CollectedHearts");
        //Debug.Log(CollectedHearts);
    }

    public void SaveMultiplier(){
        int HighestMultiplier = PlayerPrefs.GetInt("HighestMultiplier");
        if (multiplier > HighestMultiplier){
            PlayerPrefs.SetInt("HighestMultiplier",(int) HighestMultiplier);
        }
        //Debug.Log(PlayerPrefs.GetInt("HighestMultiplier"));
    }

    // public void killPlayer()
    // {
    //     //SaveScore();
    //     //SaveCoins();
    //     //player.GetComponent<PlayerController>().isDead = true;
    //     player.GetComponent<PlayerController>().isDead = true;
    //     player.GetComponent<PlayerController>().movementSettings.forwardVelocity = 0;
    //     player.GetComponent<PlayerController>()._animator.SetTrigger("Die");
    //     //_animator.SetTrigger("Die");
    //     GameOverUI.GetComponent<Animator>().SetTrigger("Show");

    // }
}
