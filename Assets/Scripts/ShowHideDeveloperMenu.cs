 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideDeveloperMenu : MonoBehaviour
{
    private bool _Show = true;

    public void Show_Hide(){
        
        if(_Show){
            GetComponent<Animator>().SetTrigger("show");
            _Show = false;
        }
        else {
            GetComponent<Animator>().SetTrigger("hide");
            _Show = true;
        }
    }
}
