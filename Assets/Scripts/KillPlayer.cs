﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour
{
    void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "Player")
        {
            //Debug.Log("Collide");
            if (!other.collider.GetComponent<PlayerController>().isDead)
            {
                other.collider.GetComponent<PlayerController>().killPlayer();
            }
        }
    }
}
