using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelRepeater : MonoBehaviour
{
    public GameObject newEnviro;
    public GameObject currentEnviro;
    private GameObject oldEnviro;

    void OnTriggerEnter(Collider other) {
        if (other.tag == "start") {
            if (oldEnviro != null) {
                Destroy (oldEnviro);
            } else {
                Destroy (GameObject.Find ("enterance"));
            }
        } else if (other.tag == "middle") {
            SpawnLevel ();
        }
    }

    void SpawnLevel () {
        oldEnviro = currentEnviro; 
        //currentEnviro = (GameObject)Instantiate (newEnviro, currentEnviro.transform.GetChild (0).position, Quaternion.identity);
        currentEnviro = (GameObject)Instantiate (newEnviro, currentEnviro.transform.GetChild (10).position, Quaternion.identity);
    }
}
