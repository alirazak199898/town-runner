using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;

    public void showLeaderboard() 
    {
        SceneManager.LoadScene(2);
    }

    public void PauseGame () 
    {
        pauseMenu.SetActive (true);
        Time.timeScale = 0;
    }

    public void ResumeGame ()
    {
        pauseMenu.SetActive (false);
        Time.timeScale = 1;
    }

    public void RestartGame ()
    {
        Application.LoadLevel (Application.loadedLevel);
        Time.timeScale = 1;
    }

    public void ContinueGame ()
    {
        Application.LoadLevel (Application.loadedLevel);
        Time.timeScale = 1;
    }

    public void QuitGame ()
    {
        Application.Quit ();
    }

    public void LeaderboardChange ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
