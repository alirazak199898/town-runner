﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    private Transform _player;
    public int speed = 10 ;
    public int distance = 100;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player").transform;
        int r = Random.Range(1, 3);
        if(r == 1)
        {
            transform.position = new Vector3(-4, transform.position.y, transform.position.z);
        }
        else if(r==2)
        {
            transform.position = new Vector3(0, transform.position.y, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(4, transform.position.y, transform.position.z);
        }
    }

    private void FixedUpdate()
    {
        if(!GameObject.Find("Player").GetComponent<PlayerController>().isDead)
        {
            if (Vector3.Distance(transform.position, _player.position) <= distance)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - speed * Time.deltaTime);
            }
        }

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
