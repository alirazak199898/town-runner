﻿using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [System.Serializable]
    public class MovementSettings
    {
        public float forwardVelocity = 10;
        public float jumpVelocity = 20;
    }

    [System.Serializable]
    public class PhysicsSettings
    {
        public float downAccel = 0.75f;
    }

    public MovementSettings movementSettings = new MovementSettings();
    public PhysicsSettings physicsSettings = new PhysicsSettings();

    public Vector3 _velocity;
    public float xSpeed = 10;
    public bool isDead = false;
    public GameObject GameOverUI;

    private Rigidbody _rigidbody;
    private Animator _animator;
    private int _jumpInput = 0, _slideInput =0;
    private bool _onGround = false;
    private float _xMovement;

    private CapsuleCollider _collider;
    private float _colliderSize;
    private float _colliderHeight;
    private Vector3 _colliderCenter; 
    public float maxSpeed;

    private string laneChange = "n";
    private int total = 0;
    private int score;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _animator  = GetComponent<Animator>();
        _velocity = Vector3.zero;

        _collider = GetComponent<CapsuleCollider>();
        _colliderHeight = _collider.height;
        _colliderCenter = _collider.center;
    }

    private void FixedUpdate()
    {
        InputHandling();
        TouchHandling();
        Run();
        CheckGround();
        Jump();
        MoveX();
        Slide();
        _rigidbody.velocity = _velocity;
    }

    void ColliderHandeling()
    {
        _colliderSize = _animator.GetFloat("ColliderSize");
        if (_colliderSize > 0.3f & _colliderSize < 1)
        {
            _collider.height = 1;
            _collider.center = new Vector3(_collider.center.x, 0.5f, _collider.center.z);

        }
        else
        {
            _collider.height = _colliderHeight;
            _collider.center = _colliderCenter;
        }
    }

    void Run()
    {
        _velocity.z = movementSettings.forwardVelocity;

        //if (movementSettings.forwardVelocity < maxSpeed)
        //{
        //    movementSettings.forwardVelocity += 0.2f * Time.deltaTime;
        //}  

        //Debug.Log(coincon.totalCoins);

        if ((coincon.totalCoins % 5 == 0) && (total == 0) && (coincon.totalCoins > 4))
        {
            movementSettings.forwardVelocity += 5;
            total = 1;
        }

        if ((coincon.totalCoins % 5 != 0) && (total == 1) && (coincon.totalCoins > 4))
        {
            total = 0;
        }

    }

    void Slide()
    {
        if(_slideInput == 1 && _onGround)
        {
            _animator.SetTrigger("Slide");
            _slideInput = 0;
        }
    }
    void Jump()
    {
        if(_jumpInput == 1 && _onGround)
        {
            _velocity.y = movementSettings.jumpVelocity;
            _animator.SetTrigger("Jump");
        }else if(_jumpInput == 0 && _onGround)
        {
            _velocity.y = 0;
        }
        else
        {
            _velocity.y -= physicsSettings.downAccel;
        }
        _jumpInput = 0;
    }

    void CheckGround()
    {
        Ray ray = new Ray(transform.position + Vector3.up * 0.1f, Vector3.down);
        RaycastHit[] hits = Physics.RaycastAll(ray, 0.5f);
        _onGround = false;
        _rigidbody.useGravity = true;
        foreach(var hit in hits)
        {
            if (!hit.collider.isTrigger)
            {
                if(_velocity.y <= 0)
                {
                    _rigidbody.position = Vector3.MoveTowards(_rigidbody.position, new Vector3(hit.point.x, hit.point.y + 0.1f, hit.point.z), Time.deltaTime * 10);
                }
                _rigidbody.useGravity = false;
                _onGround = true;
                break;
            }
        }
    }

    void MoveX()
    {
        
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(_xMovement, transform.position.y, transform.position.z),Time.deltaTime *xSpeed);
    }

    void InputHandling()
    {
        if (Input.GetKeyDown(KeyCode.Space)) //jump
        {
            _jumpInput = 1;
        }

        if(Input.GetKeyDown(KeyCode.S))
        {
            _slideInput = 1;
        }

        if (Input.GetKeyDown(KeyCode.D) && (laneChange == "n")) //right
        {
            if(_xMovement == 0)
            {
                _xMovement = 3;
                laneChange = "y";
                StartCoroutine(stopLaneCh());
            }
            
            if(_xMovement == -3)
            {
                _xMovement = 0;
                laneChange = "y";
                StartCoroutine(stopLaneCh());
            }
        }
        
        if(Input.GetKeyDown(KeyCode.A) && (laneChange == "n"))//left
        {
            if (_xMovement == 0)
            {
                _xMovement = -3;
                laneChange = "y";
                StartCoroutine(stopLaneCh());
            }
            
            if (_xMovement == 3)
            {
                _xMovement = 0;
                laneChange = "y";
                StartCoroutine(stopLaneCh());
            }
        }
    }

    void TouchHandling()
    {
        if (SwipeManager.swipeUp) //jump
        {
            _jumpInput = 1;
        }

        if(SwipeManager.swipeDown)
        {
            _slideInput = 1;
        }

        if (SwipeManager.swipeRight && (laneChange == "n")) //right
        {
            if(_xMovement == 0)
            {
                _xMovement = 3;
                laneChange = "y";
                StartCoroutine(stopLaneCh());
            }
            
            if(_xMovement == -3)
            {
                _xMovement = 0;
                laneChange = "y";
                StartCoroutine(stopLaneCh());
            }
        }
        
        if(SwipeManager.swipeLeft && (laneChange == "n"))//left
        {
            if (_xMovement == 0)
            {
                _xMovement = -3;
                laneChange = "y";
                StartCoroutine(stopLaneCh());
            }
            
            if (_xMovement == 3)
            {
                _xMovement = 0;
                laneChange = "y";
                StartCoroutine(stopLaneCh());
            }
        }
    }

    IEnumerator stopLaneCh()
    {
        yield return new WaitForSeconds(0.01f);
        _velocity.z = movementSettings.forwardVelocity;
        laneChange = "n";
    }

    public void killPlayer()
    {
        LevelManager.instance.SaveScore();
        LevelManager.instance.SaveCoins();
        LevelManager.instance.SaveHearts();
        LevelManager.instance.SaveMultiplier();
        isDead = true;
        movementSettings.forwardVelocity = 0;
        _animator.SetTrigger("Die");
        GameOverUI.GetComponent<Animator>().SetTrigger("Show");

        //SaveCoins();
    }
}
