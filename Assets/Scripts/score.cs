using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;

public class score : MonoBehaviour
{
    private double point = 0;
    //public Text scoreText;

    const string URL = "http://api.tenenet.net";
    const string token = "1dcc9e5923f189c907ec5a837d430a92";
    private string playerName = Manu.alias;
    private string id = "scoreMetric";
    private string response;


    void Update()
    {
        point = LevelManager.scoreP;
        //Debug.Log(point);
        //scoreText.text = point.ToString();
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "obstacle")
        {
            Debug.Log("Ouch!");
            StartCoroutine(highestScore(playerName, point));
        }
    }

    // private void OnTriggerEnter(Collider other)
    // {
    //     if (other.tag == "obstacle")
    //     {
    //         //Time.timeScale = 0;
    //         Debug.Log("Ouch!");
    //         StartCoroutine(highestScore(playerName, point));
    //         //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    //     }

    // }

    private IEnumerator highestScore(string name, double score)
        {
            UnityWebRequest www = UnityWebRequest.Get(URL + "/getPlayer" + "?token=" + token + "&alias=" + name);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                response = www.downloadHandler.text;
                Debug.Log(response);
            }
            var scoreJson = JsonConvert.DeserializeObject<gpRoot>(response);
            var highest = scoreJson.message.score[0].value;
            int intHighest = int.Parse(highest);
            if (score > intHighest) StartCoroutine(UpdateScore(name, score));
        }
    private IEnumerator UpdateScore(string name, double skor)
        {
            UnityWebRequest www = UnityWebRequest.Get(URL + "/insertPlayerActivity" + "?token=" + token + "&alias=" + name + "&id=" + id + "&operator=" + "add" + "&value=" + skor);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
        }

    public class gpScore
        {
            public string metric_id { get; set; }
            public string metric_name { get; set; }
            public string metric_type { get; set; }
            public string value { get; set; }
        }
    public class gpMessage
        {
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string alias { get; set; }
            public string id { get; set; }
            public string created { get; set; }
            public List<gpScore> score { get; set; }
        }
    public class gpRoot
        {
            public gpMessage message { get; set; }
            public string status { get; set; }
        }
}
